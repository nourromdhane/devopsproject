package tn.esprit.spring.services;


import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.TimesheetRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Service
public class EmployeServiceImpl implements IEmployeService {

	@Autowired
	EmployeRepository employeRepository;
	@Autowired
	DepartementRepository deptRepoistory;
	@Autowired
	ContratRepository contratRepoistory;
	@Autowired
	TimesheetRepository timesheetRepository;

	Logger loggerA = LogManager.getLogger(EmployeServiceImpl.class);
	private static final Logger LOG = LogManager.getLogger(EmployeServiceImpl.class);

	public int ajouterEmploye(Employe employe) {
		loggerA.info("*****save employe*****");
		employeRepository.save(employe);
		loggerA.info("*****Employe saved with success*****");
		return employe.getId();
	}  

	//me
	public void mettreAjourEmailByEmployeId(String email, int employeId) {
		   loggerA.info("Start mettreAjourEmailByEmployeId");
		   Optional<Employe> empl = employeRepository.findById(employeId);
	        Employe employe = null;
	        if (empl.isPresent()) {
	            employe = empl.get();
	            LOG.info(MessageFormat.format("Employe Id,{0}", employe.getId()));
	            employe.setEmail(email);
	            LOG.info(MessageFormat.format("Employe Id   {0}", employe));
	            employeRepository.save(employe);
	        } else {
	            LOG.info(MessageFormat.format("Employe doesn''t exist{0}", employe));

	        }
	}

    @Transactional
	public void affecterEmployeADepartement(int employeId, int depId) {
    	Optional<Departement> depManagedEntity = deptRepoistory.findById(depId);
		Optional<Employe> employeManagedEntity = employeRepository.findById(employeId);
		Departement dep;
		Employe emp = new Employe();

		if (depManagedEntity.isPresent()) {

			dep = depManagedEntity.get();
			if (dep.getEmployes() == null) {

				if (employeManagedEntity.isPresent()) {
					emp = employeManagedEntity.get();
					List<Employe> employes = new ArrayList<>();
					employes.add(emp);
					dep.setEmployes(employes);
				}

			} else {

				dep.getEmployes().add(emp);
			}
			loggerA.info("*****Employe added successfully to Departement*****");
			deptRepoistory.save(dep);

		}


	}
    @Transactional
	public void desaffecterEmployeDuDepartement(int employeId, int depId) {
	 LOG.info(MessageFormat.format("Department Id{0}", depId));
    LOG.info(MessageFormat.format("Employe Id , {0}", employeId));
    Departement dep = null;
    Optional<Employe> empl = employeRepository.findById(employeId);
    Optional<Departement> departement = deptRepoistory.findById(employeId);
    if (departement.isPresent() && empl.isPresent()) {
        dep = departement.get();
        LOG.debug(MessageFormat.format("Department Id{0}", depId));

        int employeNb = dep.getEmployes().size();
        for (int index = 0; index < employeNb; index++) {
            if (dep.getEmployes().get(index).getId() == employeId) {
                dep.getEmployes().remove(index);
                LOG.debug(MessageFormat.format("Employe Id  ,{0}", employeId));

                break;//a revoir
            }
        }
    }

	}

	//mee
	public int ajouterContrat(Contrat contrat) {
		loggerA.info("Start ajouterContrat");
		LOG.info(MessageFormat.format("Contrat{0}", contrat));

        contratRepoistory.save(contrat);
        LOG.debug(MessageFormat.format("Contrat saved{0}", contrat));
        LOG.debug(MessageFormat.format("Contrat Reference{0}", contrat.getReference()));
        return contrat.getReference();
	}

	//me
	public void affecterContratAEmploye(int contratId, int employeId) {
		  LOG.info(MessageFormat.format("Employe Id {0}", employeId));
	        LOG.info(MessageFormat.format("Contrat Id {0}", contratId));
	        Contrat contratManagedEntity = null;
	        Employe employeManagedEntity = null;
	        Optional<Employe> empl = employeRepository.findById(employeId);
	        Optional<Contrat> contrat = contratRepoistory.findById(employeId);
	        if (contrat.isPresent() && empl.isPresent()) {
	            contratManagedEntity = contrat.get();
	            LOG.debug(MessageFormat.format("Contrat Id{0}", contratId));

	            employeManagedEntity = empl.get();
	            LOG.debug(MessageFormat.format("Employe Id{0}", employeId));

	            contratManagedEntity.setEmploye(employeManagedEntity);
	            LOG.info(MessageFormat.format("Contrat added to Employe{0}", employeId));

	            contratRepoistory.save(contratManagedEntity);
	            LOG.info(MessageFormat.format("Contrat{0}", contratManagedEntity));
	        } else {
	            LOG.info("Employe or Contrat doesn't exist");
	        }


	}

    public String getEmployePrenomById(int employeId) {
    	loggerA.info("***start get Employe Prenom By Id");
		Optional<Employe> employeManagedEntity = employeRepository.findById(employeId);
		Employe emp = new Employe();
		if (employeManagedEntity.isPresent()) {
			emp = employeManagedEntity.get();
		}
		return emp.getPrenom();
    }
    public void deleteEmployeById(int employeId) {
    	Employe employe = null;
		Optional<Employe> emp = employeRepository.findById(employeId);
		if (emp.isPresent()) {
			employe = emp.get();
			// Desaffecter l'employe de tous les departements
			// c'est le bout master qui permet de mettre a jour
			// la table d'association
			for (Departement dep : employe.getDepartements()) {
				dep.getEmployes().remove(employe);
			}
			loggerA.info("***Employe deleted with success***");
			employeRepository.delete(employe);
		}

	}

	//me
	public void deleteContratById(int contratId) {
		 LOG.info(MessageFormat.format("Contrat Id{0}", contratId));
	        Optional<Contrat> contrat = contratRepoistory.findById(contratId);
	        Contrat contratManagedEntity = null;
	        if (contrat.isPresent()) {
	            contratManagedEntity = contrat.get();
	            contratRepoistory.delete(contratManagedEntity);
	            LOG.info(MessageFormat.format("Contrat Id deleted{0}", contratId));
	        } else {
	            LOG.info("Contrat doesn't exist");

	        }

		}
	//me
	public int getNombreEmployeJPQL() {
		loggerA.info("Start getNombreEmployeJPQ");
		loggerA.info("Liste employees" + employeRepository.countemp());
		int count= employeRepository.countemp();
		loggerA.info("End getNombreEmployeJPQ");
		loggerA.error("Cannot find NombreEmployeJPQL");
		return count;
	}

	
	public List<String> getAllEmployeNamesJPQL() {
		return employeRepository.employeNames();

	}
	
	public List<Employe> getAllEmployeByEntreprise(Entreprise entreprise) {
		return employeRepository.getAllEmployeByEntreprisec(entreprise);
	}

	public void mettreAjourEmailByEmployeIdJPQL(String email, int employeId) {
		employeRepository.mettreAjourEmailByEmployeIdJPQL(email, employeId);
		loggerA.info("***Employe's email updated with success***");

	}
	public void deleteAllContratJPQL() {
         employeRepository.deleteAllContratJPQL();
	}
	
	public float getSalaireByEmployeIdJPQL(int employeId) {
		return employeRepository.getSalaireByEmployeIdJPQL(employeId);
	}

	public Double getSalaireMoyenByDepartementId(int departementId) {
		return employeRepository.getSalaireMoyenByDepartementId(departementId);
	}
	
	public List<Timesheet> getTimesheetsByMissionAndDate(Employe employe, Mission mission, Date dateDebut,
			Date dateFin) {
		loggerA.info("***get timesheets By Mission And Date***");
		return timesheetRepository.getTimesheetsByMissionAndDate(employe, mission, dateDebut, dateFin);
		
	}

	//me
	public List<Employe> getAllEmployes() {
		loggerA.info("Start getAllEmployes()");
		List<Employe> list= (List<Employe>) employeRepository.findAll();
		loggerA.info("End getAllEmployes=()");

		return list;

	}

	 @Override
	    public int addOrUpdateEmploye(Employe employe) {

	        employeRepository.save(employe);
	        return employe.getId();
	    }

	@Override
	public Contrat getContratById(int contratId) {
		return contratRepoistory.findById(contratId).orElse(null);
	}

	@Override
	public List<Contrat> getAllContrats() {
		loggerA.info("Start getAllContrats()");
		List<Contrat> list= (List<Contrat>) contratRepoistory.findAll();
		loggerA.info("End getAllContrats=()");

		return list;
	}

	@Override
	public Employe findEmployeById(int id) {
		return employeRepository.findById(id).orElse(null);
	}
	
	


}
