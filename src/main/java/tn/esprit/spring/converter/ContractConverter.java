package tn.esprit.spring.converter;

import org.springframework.stereotype.Component;

import tn.esprit.spring.dto.ContractDTO;
import tn.esprit.spring.entities.Contrat;

@Component
public class ContractConverter {

	public Contrat dtoToEntity(ContractDTO dto) {
		Contrat contrat = new Contrat();
		
		contrat.setDateDebut(dto.getDateDebut());
		contrat.setEmploye(dto.getEmploye());
		contrat.setReference(dto.getReference());
		contrat.setTypeContrat(dto.getTypeContrat());
		return contrat;
	}
}
