package tn.esprit.spring;
import org.junit.Test;

import tn.esprit.spring.utils.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.Assert;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.services.EmployeServiceImpl;
import tn.esprit.spring.services.IEmployeService;
import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.repository.EmployeRepository;
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeServiceImplTest  extends BaseJUnit49TestCase  {
	private static final Logger LOG = LogManager.getLogger(EmployeServiceImplTest.class);
	
	@Autowired
	EmployeRepository employeRepository;
	@Autowired
	IEmployeService employeservice ; 
	@Autowired
	ContratRepository contratRepoistory;
	private Employe employe;
	private Contrat contrat;

	@Override
	public void setUp() throws Exception {
		super.setUp();
		this.employe = new Employe();
		this.employe.setPrenom("safwen");
		this.employe.setNom("saf");
		this.employe.setEmail("safwen.bentili@esprit.tn");
		this.employe.setActif(true);
		this.employe.setRole(Role.INGENIEUR);
		this.contrat = new Contrat();
		

	}

	/*@Test
	public void tests() throws ParseException {
		ajouterContratTest();
		affecterContratAEmployeTest();
		//deleteContratByIdTest();
		mettreAjourEmailByEmployeIdTest();
		
	}
	public void ajouterContratTest() throws ParseException{
		Employe employe= new Employe();
		employe.setActif(false);
		employe.setEmail("ameddeb@vermeg.com");
		employe.setNom("ahlem");
		employe.setPrenom("meddeb");
		
		Contrat contrat = new Contrat();
		contrat.setReference(new Random().nextInt(999));
		SimpleDateFormat df = new SimpleDateFormat("yyyy/mm/dd");
		Date datedebut = df.parse("2022/02/01");
		contrat.setDateDebut(datedebut);
		contrat.setEmploye(employe);
		contrat.setSalaire(100);
		contrat.setTypeContrat("travail");
		
		employeservice.ajouterEmploye(employe);
		int idocntract = employeservice.ajouterContrat(contrat);
		Optional<Contrat> ctr = contratRepoistory.findById(idocntract);
		if(ctr.isPresent()) {
			assertEquals(idocntract, contrat.getReference());
		}		
	}
	public void affecterContratAEmployeTest() throws ParseException {
	Employe employe= new Employe();
	employe.setActif(false);
	employe.setEmail("efsffs");
	employe.setNom("aaaa");
	employe.setPrenom("sfsfe");
	
	Contrat contrat = new Contrat();
	contrat.setReference(new Random().nextInt(999));
	SimpleDateFormat df = new SimpleDateFormat("yyyy/mm/dd");
	Date datedebut = df.parse("2022/02/01");
	contrat.setDateDebut(datedebut);
	contrat.setEmploye(employe);
	contrat.setSalaire(100);
	contrat.setTypeContrat("CDI");
	
	employeservice.ajouterEmploye(employe);
	
	
	employeservice.affecterContratAEmploye(contrat.getReference(), employe.getId());
	Contrat ctr = employeservice.getContratById(contrat.getReference());
		
	assertNull(ctr);
}*/

	
	
	/*@Test
	public void getAllEmployesTest() {
		List<Employe> emps = employeservice.getAllEmployes();
		Assert.assertNotNull(emps);
	}
	@Test
	public void getNombreEmployeJPQLTest() {
		Integer countEmp = employeservice.getNombreEmployeJPQL();
		Assert.assertNotNull(countEmp);
	}*/
	@Test
	public void mettreAjourEmailByEmployeIdTest() {
		Employe emp = new Employe();
		String email = "MyMail@myDomain.com";
		employeservice.ajouterEmploye(emp);
		employeservice.mettreAjourEmailByEmployeId(email, emp.getId());
		Employe assetEmp = employeservice.findEmployeById(emp.getId());
		Assert.assertEquals(assetEmp.getEmail(), email);
		employeservice.deleteEmployeById(emp.getId());
	}
	/*@Test
	public void deleteContratByIdTest() throws ParseException {
		Contrat contrat = new Contrat();
		contrat.setReference(new Random().nextInt(999));
		SimpleDateFormat df = new SimpleDateFormat("yyyy/mm/dd");
		Date datedebut = df.parse("2022/02/01");
		contrat.setDateDebut(datedebut);
		contrat.setEmploye(null);
		contrat.setSalaire(100);
		contrat.setTypeContrat("CDI");
		
		int idocntract = employeservice.ajouterContrat(contrat);
		
		employeservice.deleteContratById(idocntract);
		Optional<Contrat> ctr = contratRepoistory.findById(contrat.getReference());
		assertEquals(false, ctr.isPresent());
		
	}*/
}
	


