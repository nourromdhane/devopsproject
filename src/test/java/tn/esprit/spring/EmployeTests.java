package tn.esprit.spring;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.expression.ParseException;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.MissionRepository;
import tn.esprit.spring.services.IEmployeService;
import tn.esprit.spring.services.IEntrepriseService;
import tn.esprit.spring.services.TimesheetServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class EmployeTests {

	@Autowired
	IEmployeService employeservice;
	@Autowired
	MissionRepository missionrepo;
	@Autowired
	EmployeRepository employerepo;
	@Autowired
	IEntrepriseService entrepriseservice;
	@Autowired
	TimesheetServiceImpl timesheetservice;
	public static final Logger loggerA = LogManager.getLogger(EmployeTests.class);

	@Test
	public void contextLoads() throws Exception {
		assertThat(employeservice).isNotNull();
	}
	@Test
	public void tests() throws Exception {
		testAjouterEmploye();
		testAffecterEmployeADepartement();
		testGetEmployePrenomById();
		testDeleteEmployeById();
		testGetEmployePrenomById();
		testDeleteEmployeById();
		testMettreAjourEmailByEmployeIdJPQL();		
	}

	
	public void testAjouterEmploye() {
		Employe emp = new Employe("feriel", "bentlili", "feriel.bentlili@esprit.tn", true, Role.INGENIEUR);
		int courant = employeservice.getAllEmployes().size();
		employeservice.ajouterEmploye(emp);
		assertEquals(courant + 1, employeservice.getAllEmployes().size());
	}

	
	public void testAffecterEmployeADepartement() {
		Employe emp = new Employe("Feriel", "bentlili", "feriel.bentlili@gmail.com", true, Role.CHEF_DEPARTEMENT);
		employerepo.save(emp);
		int idemp = employeservice.ajouterEmploye(emp);
		Departement departement = new Departement("Spring");
		int idD = entrepriseservice.ajouterDepartement(departement);
		employeservice.affecterEmployeADepartement(idemp, idD);
		assertEquals(idD, departement.getId());
	}
	

	
	public void testGetEmployePrenomById() {
		try {
			int idEmploye = employeservice
					.ajouterEmploye(new Employe("youssef", "karoui", "youssef.karoui@spring.tn", true, Role.TECHNICIEN));
			String prenomEmp = employeservice.getEmployePrenomById(idEmploye);
			loggerA.info("Prenom de lemploye est : " + prenomEmp);
			assertThat(prenomEmp).isEqualTo("karoui");
			employeservice.deleteEmployeById(idEmploye);
		} catch (Exception e) {
			loggerA.error(String.format("Erreur dans Get EmployePrenom By Id : %s ", e));

		}

	}

	
	public void testDeleteEmployeById() {
		Employe employee = new Employe();
		employee.setEmail("omarfraj@gmail.com");
		employee.setNom("omar");
		employee.setPrenom("");
		employee.setRole(Role.INGENIEUR);
		employee.setActif(true);
		employeservice.ajouterEmploye(employee);
		if (employerepo.findById(employee.getId()).isPresent()) {
			employeservice.deleteEmployeById(employee.getId());
			assertTrue(true);
			log.info("Employee deleted with success");

		} else {
			assertTrue(false);
			log.info("Delete : Employee deletion failed");

		}

	}

	
	public void testMettreAjourEmailByEmployeIdJPQL() throws Exception {
		Employe emp = new Employe();
		emp.setEmail("omarfraj@gmail.com");
		emp.setNom("omar");
		emp.setPrenom("");
		emp.setRole(Role.INGENIEUR);
		emp.setActif(true);
		int idEmploye = employeservice
				.ajouterEmploye(emp);
		String newemail = "feriel.bentlili@esprit.tn";
		employeservice.mettreAjourEmailByEmployeId(newemail,idEmploye);
		Optional<Employe> e = employerepo.findById(idEmploye);
		if (e.isPresent()) {
			assertNotEquals("your new email is updated", emp.getEmail(), newemail);
		}
	}

	public void testGetTimesheetsByMissionAndDate() throws ParseException, Exception {
		SimpleDateFormat df = new SimpleDateFormat("yyyy/mm/dd");
		Date datedebut = df.parse("2022/02/01");
		Date datefin = df.parse("2022/02/27");
		Employe employee = new Employe();
		employee.setEmail("feriel@gmail.com");
		employee.setNom("feriel");
		employee.setPrenom("bentlili");
		employee.setRole(Role.INGENIEUR);
		employee.setActif(true);
		employeservice.ajouterEmploye(employee);
		Mission mission =new Mission("missiondevops", "cette mission a pour but d'avoir plus de compétences en devops") ;
		timesheetservice.ajouterMission(mission);
		employeservice.getTimesheetsByMissionAndDate(employee, mission, datedebut, datefin);
		assertTrue(true);
	}

}
