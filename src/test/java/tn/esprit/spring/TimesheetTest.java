package tn.esprit.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.MissionRepository;
import tn.esprit.spring.repository.TimesheetRepository;
import tn.esprit.spring.services.IEmployeService;
import tn.esprit.spring.services.ITimesheetService;
import tn.esprit.spring.utils.BaseJUnit49TestCase;

public class TimesheetTest extends BaseJUnit49TestCase {
	private static final Logger LOG = LogManager.getLogger(TimesheetTest.class);

	@Autowired
	ITimesheetService timesheetService;
	@Autowired
	TimesheetRepository timesheetRepo;
	@Autowired
	MissionRepository missionRepo;
	@Autowired
	DepartementRepository departRepo;
	@Autowired
	EmployeRepository employeRepo;
	@Autowired
	IEmployeService employeService;
	private Mission mission;
	private Employe employe;
	private Employe chefDepartement;
	private Departement departement;

	@Override
	public void setUp() throws Exception {
		super.setUp();
		this.mission = new Mission();
		this.mission.setName(getIdHelper().createRandomString(5));
		this.mission.setDescription(getIdHelper().createRandomString(20));
		this.mission.setId(getIdHelper().createRandomInt());
		this.departement = new Departement();
		this.departement.setId(getIdHelper().createRandomInt());
		this.departement.setName(getIdHelper().createRandomString(5));

		this.employe = new Employe();
		this.employe.setId(getIdHelper().createRandomInt());
		this.employe.setNom(getIdHelper().createRandomString(5));
		this.employe.setPrenom(getIdHelper().createRandomString(5));
		this.employe.setEmail(getIdHelper().createRandomString(12));
		this.employe.setActif(true);
		this.employe.setRole(Role.INGENIEUR);

		this.chefDepartement = new Employe();
		this.chefDepartement.setId(getIdHelper().createRandomInt());
		this.chefDepartement.setNom(getIdHelper().createRandomString(5));
		this.chefDepartement.setPrenom(getIdHelper().createRandomString(5));
		this.chefDepartement.setEmail(getIdHelper().createRandomString(12));
		this.chefDepartement.setActif(true);
		this.chefDepartement.setRole(Role.CHEF_DEPARTEMENT);

	}

	@Test
	public void testAjouterMission() {
		Mission mission = new Mission();
		mission.setId(getIdHelper().createRandomInt());
		int initialSize = (int) missionRepo.count();
		LOG.info("**************************" + "nombre de ligne avant insertion = " + initialSize);
		timesheetService.ajouterMission(mission);
		LOG.info("**************************" + " insertion");
		int sizeAfterInsert = (int) missionRepo.count();
		LOG.info("**************************" + "nombre de ligne après insertion = " + sizeAfterInsert);
		assertTrue(sizeAfterInsert > initialSize);

	}

	@Test
	public void testAffecterMissionADepartement() {
		Mission mission = new Mission();
		mission.setId(getIdHelper().createRandomInt());
		Departement departement = new Departement();
		departement.setId(getIdHelper().createRandomInt());
		departement.setName("INFO");
		departRepo.save(departement);
		mission = missionRepo.save(mission);
		departement = departRepo.save(departement);
		timesheetService.affecterMissionADepartement(mission.getId(), departement.getId());
		int depId = missionRepo.findById(mission.getId()).orElse(null).getDepartement().getId();
		assertEquals(depId, departement.getId());
		LOG.info("**************************" + "departement.getId() = " + departement.getId());
	}

	@Test
	public void testAjouterTimesheet() {
		Iterable<Mission> missions = missionRepo.findAll();
		Mission mission;
		if (!missions.iterator().hasNext()) {
			LOG.warn("Création d'une mission");
			mission = new Mission();
			mission.setId(getIdHelper().createRandomInt());
			mission.setName("Organisateur");
			missionRepo.save(mission);
		} else {
			mission = missions.iterator().next();
		}
		Iterable<Employe> employes = employeRepo.findAll();
		Employe emp;
		if (!employes.iterator().hasNext()) {
			LOG.warn("Création d'un employé");
			emp = new Employe();
			emp.setEmail("ilengliz@gmail.com");
			emp.setNom("Lengliz");
			emp.setPrenom("Ibtissem");
			emp.setId(getIdHelper().createRandomInt());
			employeRepo.save(emp);
		} else {
			emp = employes.iterator().next();
		}

		LOG.warn("Création d'un timesheet");
		Date startDate = new Date(2021, 2, 19);
		Date endDate = new Date(2026, 2, 19);
		long initialSize = timesheetRepo.count();
		LOG.info("initial Size  ajouter timesheet" + initialSize);
		timesheetService.ajouterTimesheet(mission.getId(), emp.getId(), startDate, endDate);
		long sizeAfterInsert = timesheetRepo.count();
		LOG.info(" Size  after insert ajouter timesheet" + sizeAfterInsert);
		assertTrue(initialSize < sizeAfterInsert);
	}


}